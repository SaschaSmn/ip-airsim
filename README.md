## Get the project
- Clone the project via **https://gitlab.com/SaschaSmn/ip-airsim**

## Install Libraries
- If you use venv, type this in your console:  
pip install -r requirements.txt 

- If you use conda, then:  
conda env create -f environment.yml  
conda activate "env name"

## Run your Project
- To finally run your project, place your neural network 
in the neural_nets folder and specify the path of your network in the student_task_neural_network.py down below in the main

- You can choose to opt out certain options:
    - livefeed_car: window that shows camera perspective from the cars front view. Bounding boxes of the detected object will be shown too
    - livefeed_sign: by one window per detected object/sign will pop up/close if no longer in the view of the car
    - classify_signs: the student model classifies the sign. Either prints out or changes the title of the window (if livefeed_sign is enabled too) with the information being the class of and the confidence of the model, that said object is of certain class
    - yolo_detect_output & yolo_classification_output: verbose information that the yolov8 model always prints out, when tasked to predict

## How this works
We build a neural network that is able to detect certain objects.
In your case, our model detects signs (mine is able to detect cars).
The car can be driven around in the airsim environment.
While driving, we can take pictures.
These pictures are now put into
our neural network.
This results in an analysed image with some resulting data.
For example, in this data we find coordinates where the object is located in the image or the confidence of the model,
that it detected the object in the right way
(or how sure the model is that there is an object in this location anyway).
We can now use this data to forward the image of the object (and only the object)
to your neural network.  
Imagine our car is driving around while driving our model is looking for traffic signs.
Now it sees a traffic sign.
The Image will be cropped, so that only the part of the image with the sign in it will be used.
After that your model comes into play.
It will be tasked to analyse the cropped image.
Its task is now to classify what kind of street sign was detected.
The results can either be printed out in the command line or the window title will contain certain information.

