import cv2
import os
import airsim
import numpy as np
from ultralytics import YOLO


class StreetSignAnalyser:

    def __init__(self, sign_detect_neural_net: YOLO, sign_classification_neural_net: YOLO = None,
                 classify_signs: bool = True,
                 livefeed_car: bool = False, livefeed_sign: bool = True):
        # params
        self.sign_detect_neural_net = sign_detect_neural_net
        self.sign_classification_neural_net = sign_classification_neural_net
        self.classify_signs = classify_signs
        self.livefeed_car = livefeed_car
        self.livefeed_sign = livefeed_sign

        # verbose output of neural network (yolo model)
        self.yolo_detect_output = False
        self.yolo_classification_output = False

    # From a logical standpoint the given image should only contain one object e.g. the street sign.
    # But if multiple objects were detected in one object 'bounding box',
    # then multiple confidence scores could be given out. That is because all detected objects will be analysed.
    # To deal with this 'issue' only the first found object will be the chosen one.
    def analyse_sign(self, images):
        for i, img in enumerate(images):
            # analyse the image
            results = self.sign_classification_neural_net(img, verbose=self.yolo_classification_output)
            object_name, object_confidence = '-', 0.0
            boxes = results[0].boxes
            # if an object/sign was detected
            if boxes:
                box = boxes[0]
                object_name = results[0].names[(int(box.cls[0]))]
                object_confidence = box.conf[0]
            # might be resource intensive. use print if lag occurs
            # will print out class of the object and the confidence of the neural network that this object is this certain object
            if self.livefeed_sign:
                cv2.setWindowTitle(f'Object {i}', f'{object_name} {object_confidence}%')
            else:
                print(f'Object {i}: {object_name} with {object_confidence}% confidence')

    def update(self):
        # keeps track of the number of objects that were detected in the last cycle
        cnt_det_last_run = 0
        while 1:

            # capture image
            pictures = client.simGetImages([
                # png image center
                airsim.ImageRequest(0, airsim.ImageType.Scene, False, False),
            ])
            unedited_picture = pictures[0]

            # reshape image
            img1d = np.fromstring(unedited_picture.image_data_uint8, dtype=np.uint8)
            img_rgb = img1d.reshape((unedited_picture.height, unedited_picture.width, 3))

            # detect signs in image
            results = self.sign_detect_neural_net(img_rgb, conf=0.4, verbose=self.yolo_detect_output)

            ### livefeed_car ###
            if self.livefeed_car:
                # show results in new frame
                livefeed_car_frame = results[0].plot()
                resized_car_frame = cv2.resize(livefeed_car_frame, (320, 240))
                cv2.imshow("livefeed_car", resized_car_frame)

            sign_images = []
            for box in results[0].boxes:
                # get coords of object
                x1, y1, x2, y2 = box.xyxy[0]
                sign_img = img_rgb[int(y1):int(y2), int(x1):int(x2)]
                sign_images.append(sign_img)

            ### livefeed_sign ###
            if self.livefeed_sign:
                # close old windows
                if len(sign_images) < cnt_det_last_run:
                    for i in range(cnt_det_last_run - 1, len(sign_images) - 1, -1):
                        cv2.destroyWindow(f'Object {i}')
                cnt_det_last_run = len(sign_images)

                # plot object if list is not empty (objects were detected)
                if sign_images:
                    for i, img in enumerate(sign_images):
                        resized_sign_frame = cv2.resize(img, (320, 240))
                        cv2.imshow(f'Object {i}', resized_sign_frame)

            ### analyse sign ###
            if self.classify_signs and sign_images:
                self.analyse_sign(sign_images)

            # break windows with 'q' key
            if cv2.waitKey(1) & 0xFF == ord("q"):
                cv2.destroyAllWindows()
                break


if __name__ == '__main__':
    client = airsim.CarClient()
    client.confirmConnection()

    # our neural network
    detect_model_path = os.path.normpath(r'./neural_nets')
    detect_model_name = '100e_oid_as.pt'
    detect_model = YOLO(
        os.path.join(detect_model_path, detect_model_name)
    )

    # move your nn in the neural_nets path and specify the net
    # your neural network
    analyse_model_path = os.path.normpath(r'./neural_nets')
    analyse_model_name = '100e_oid_as.pt'
    analyse_model = YOLO(
        os.path.join(analyse_model_path, analyse_model_name)
    )

    ssa = StreetSignAnalyser(detect_model, analyse_model)
    ssa.update()
